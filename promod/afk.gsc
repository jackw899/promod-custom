/*init() {
  while (true) {
    level waittill("connected", player);
    onPlayerConnected();
  }
}*/

onPlayerConnected() {
  level waittill("connected", player);
  IPrintLnBold(player.name);
  player thread afkMonitor();
}

new () {
  self endon("disconnect");
  while (true) {
    for (i = 0; i < level.players.size; i++) {
      player = level.players[i];
      if (!IsDefined(player.lastMovement)) {
        player.lastMovement = gettime();
      }
      if (isAlive(player) && player getVelocity() != 0) {
        player.lastMovement = gettime();
      }

      afkTime = (gettime() - player.lastMovement) / 1000;
      if (afkTime >= 10) {
        IPrintLnBold("U AFK?!");
      }
      IPrintLnBold(afkTime);
    }
    IPrintLnBold("Test");
    wait 1;
  }
}

afkMonitor() {
  self endon("disconnect");

  self.lastMovement = gettime();
  self.afkWarned = false;

  while (true) {
    if (isDefined(self.pers["team"]) && self.pers["team"] != "spectator") {
      if (isAlive(self) && self getVelocity() != 0) {
        self.lastMovement = gettime();
        self.afkWarned = false;
      }

      afkTime = (gettime() - self.lastMovement) / 1000;
      if (afkTime >= 10) {
        self IPrintLnBold("U AFK M8");
      }
      self IPrintLnBold(afkTime);
    }
    wait 1;
  }
}

main() {
  self endon("disconnect");
  afkKickTime = 120;
  warningTime = 110;
  warned = false;
  if (!IsDefined(self.afkTime))
    self.afkTime = 0;

  while (true) {
    /*if (IsDefined(self.sessionstate)) {
      oldScore = GetTeamScore("allies") + GetTeamScore("axis");
      oldPosition = self.origin;
      wait 1;
      newPosition = self.origin;
      newScore = GetTeamScore("allies") + GetTeamScore("axis");
      if (oldScore == newScore) {
        if (oldPosition == newPosition) {
          self.afkTime++;
        } else {
          self.afkTime = 0;
        }

        if (self.afkTime >= warningTime && !warned) {
          self IPrintLnBold("You will be kicked in 10 seconds for being
    afk.");
          warned = true;
        }
        if (self.afkTime >= afkKickTime) {
          IPrintLnBold("AFK KICK NOW");
          self suicide();
        }
      }
      IPrintLnBold(self.afkTime);
    } else {
      wait 1;
    }*/
    if (IsDefined(self.sessionstate))
      self.afkTime++;
    IPrintLnBold(self.afkTime);
    wait 1;
  }
}
